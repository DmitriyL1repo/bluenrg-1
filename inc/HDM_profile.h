#ifndef __HDM_PROFILE_H
#define __HDM_PROFILE_H

#define HDMP_MODULE_DEBUG_ENABLE 1

#if HDMP_MODULE_DEBUG_ENABLE == 1
#define PRINT_DEBUG_0(fmt)              printf(fmt);
#define PRINT_DEBUG_1(fmt,arg1)         printf(fmt, arg1);
#define PRINT_DEBUG_2(fmt,arg1,arg2)    printf(fmt, arg1, arg2);       
#else
#define PRINT_DEBUG {}
#define PRINT_DEBUG_0(fmt) {}
#define PRINT_DEBUG_1(fmt,arg1) {}
#define PRINT_DEBUG_2(fmt,arg1,arg2)  {}
#endif                


void HDM_profile_process(void);
void HDM_profile_reset(void);
void HDM_profile_OnOff_stream( uint8_t state );
uint8_t HDM_profile_is_stream_active( void );
void HDM_profile_write_remote_config( uint8_t *data, uint8_t length );
void HDM_profile_read_remote_config(void);

#endif