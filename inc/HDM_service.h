#ifndef __HDM_SERVICE_H
#define __HDM_SERVICE_H

#define HDM_MODULE_DEBUG_ENABLE 1

#if HDM_MODULE_DEBUG_ENABLE == 1
#define PRINT_DEBUG_0(fmt)              printf(fmt);
#define PRINT_DEBUG_1(fmt,arg1)         printf(fmt, arg1);
#define PRINT_DEBUG_2(fmt,arg1,arg2)    printf(fmt, arg1, arg2);                                        
#else
#define PRINT_DEBUG_0(fmt) {}
#define PRINT_DEBUG_1(fmt,arg1) {}
#define PRINT_DEBUG_2(fmt,arg1,arg2)  {}
#endif                




void HDM_service_init(void);
void HDM_service_process(void);
void HDM_service_reset(void);
uint8_t HDM_service_is_stream_active(void);
uint8_t HDM_service_send_status_data( uint8_t *data, uint8_t size );

#endif