#ifndef __CENTRAL_MNGR_APP_H
#define __CENTRAL_MNGR_APP_H

#include "peripheral_mngr_app.h"
#include "bluenrg1_stack.h"
#include "common_conf.h"

#if ROLE == CENTRAL

#define CENTRAL_MNGR_MODULE_DEBUG_ENABLE 1

//Device's working modes
#define APP_STATUS_IDLE                 (0x00)
#define APP_STATUS_SCAN                 (0x01)
#define APP_STATUS_SCAN_REPORT_RDY      (0x02)
#define APP_STATUS_CONNECTED            (0x03)

//Connection parameters
#define SCAN_INTERVAL   (0x1F40)          //(N * 0.625 msec)    5000 ms
#define SCAN_WINDOW     (0x140)           //(N * 0.625 msec)    200 ms

#define CONN_INTERVAL_MIN      (0x0006)   //(N * 0.625 msec)    7.5 ms
#define CONN_INTERVAL_MAX      (0x000C)   //(N * 0.625 msec)    15 ms
#define CONN_LATENCY           (0)
#define SUPERVISION_TIMEOUT    (4000)
#define CONN_EVENT_LEN_MIN     (2000)
#define CONN_EVENT_LEN_MAX     (2000)

#define OWN_ADDR_TYPE          (0x00) //Public address


#if CENTRAL_MNGR_MODULE_DEBUG_ENABLE == 1
#define PRINT_DEBUG_0(fmt)              printf(fmt);
#define PRINT_DEBUG_1(fmt,arg1)         printf(fmt, arg1);
#define PRINT_DEBUG_2(fmt,arg1,arg2)    printf(fmt, arg1, arg2);                                        
#else
#define PRINT_DEBUG_0(fmt) {}
#define PRINT_DEBUG_1(fmt,arg1) {}
#define PRINT_DEBUG_2(fmt,arg1,arg2)  {}
#endif                


typedef struct
{  
  uint8_t index;
  uint8_t Peer_Name[PEER_NAME_MAX_LEN];
  uint8_t Peer_Address_Type;
  uint8_t Peer_Address[6];
}scannedDeviceInfoType;

typedef union
{
  uint8_t byte;
  struct
  {    
    uint8_t scan_list_updated     :1;    
    uint8_t rfu                   :7;
  }bits;
}central_app_flags_type;


APP_Status CENTRAL_APP_Init_BLE(void);
APP_Status CENTRAL_APP_Scan(void);
APP_Status CENTRAL_APP_Connect_To_Peer( uint8_t peer_index );
void CENTRAL_APP_Disconnect( uint8_t reason );
void CENTRAL_APP_Tick(void);
uint16_t CENTRAL_APP_Get_Connection_Handle(void);
uint8_t CENTRAL_APP_Get_Connection_Status(void);
void CENTRAL_APP_remote_command_parser( uint8_t *data, uint8_t length );

#endif //ROLE == CENTRAL

#endif