#ifndef __COMM_MNGR_H
#define __COMM_MNGR_H

//This header is common for central_comm_mngr.c and peripheral_comm_mngr.c files
//See difference in function implementation

void CM_init(void);
void CM_command_monitor(void);
void CM_uart_rx_callback( uint8_t data );
void CM_uart_tx_command( uint8_t *data, uint8_t length );
void CM_reset_rx_buffer(void);
uint8_t CM_fill_stream_buffer_for_BLE( uint8_t reqiured_size, uint8_t *data );
void CM_fill_stream_buffer( uint8_t size, uint8_t *data );

#endif