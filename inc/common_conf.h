#ifndef __COMMON_CONF_H
#define __COMMON_CONF_H

typedef enum 
{
  APP_SUCCESS = 0x00, /*!< APP Success.*/
  APP_ERROR = 0x10    /*!< APP Error.*/
} APP_Status;

#define CENTRAL 0
#define PERIPHERAL 1

//------------------------------------------------------------------------------
// USER DEFINES SECTION
//------------------------------------------------------------------------------
// Herein defined parameters that can be changed by user
// Do not change any parameters that not included to current section

//Uncomment one of possible roles below
#define ROLE CENTRAL 
//#define ROLE PERIPHERAL

//Input data stream buffer size. Minimum size 32 bytes!
#define STREAM_BUFFER_SIZE  1024

//Uncomment following to enable throughput test. Recompile project for both roles.
#define THROUGHPUT_TEST_ENABLE

//---- CENTRAL ROLE RELATED SETTINGS -----

//Maximum number of peers central can scan at the time 
#define PERIPH_LIST_MAX_NUMBER  5 //shouldn't be more than 9!

//Enable/disable peers filtering by name prefix
#define PEERS_FILTERING_BY_NAME_PREFIX_ENABLED  1


//---- PERIPHERAL ROLE RELATED SETTINGS -----

//Fill last 4 characters with valid serial number.
//Do not change "HDM" prefix size since it will impact on device recognition on air!!
//#define NAME_WEAR 'H', 'D', 'M', '0', '0', '0', '2'
#define NAME_WEAR 'N', 'O', 'R', 'M', 'A', 'N', '0', '0', '0', '1'

//------------------------------------------------------------------------------
// END of USER DEFINES SECTION
//------------------------------------------------------------------------------

#ifndef ROLE
#error BLE role must be defined!
#endif


#define PEER_NAME_MAX_LEN       11      //old value 10
#define PEER_NAME_PREFIX_SIZE   6       //old value 3


enum
{
  RCOM_SET_CONF_PART1 = 100,
  RCOM_SET_CONF_PART2,
  RCOM_GET_CONF,
  RCOM_START_STREAM,
  RCOM_STOP_STREAM
};



void APP_Error_Handler(void);



#endif