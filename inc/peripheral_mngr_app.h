#ifndef __PERIPHERAL_MNGR_APP_H
#define __PERIPHERAL_MNGR_APP_H


#include "bluenrg1_stack.h"
#include "bluevoice_app.h"
#include "common_conf.h"

#if ROLE == PERIPHERAL

#define PER_MNGR_MODULE_DEBUG_ENABLE 1

#if PER_MNGR_MODULE_DEBUG_ENABLE == 1
#define PRINT_DEBUG_0(fmt)              printf(fmt);
#define PRINT_DEBUG_1(fmt,arg1)         printf(fmt, arg1);
#define PRINT_DEBUG_2(fmt,arg1,arg2)    printf(fmt, arg1, arg2);       
#else
#define PRINT_DEBUG {}
#define PRINT_DEBUG_0(fmt) {}
#define PRINT_DEBUG_1(fmt,arg1) {}
#define PRINT_DEBUG_2(fmt,arg1,arg2)  {}
#endif                

#define APP_STATUS_ADVERTISEMENT                        (0x10)        /*!< BlueVoice Peripheral device is in advertisement mode.*/
#define APP_STATUS_CONNECTED                            (0x20)        /*!< BlueVoice device is connected.*/

/* Exported variables --------------------------------------------------------*/
extern volatile uint8_t APP_PER_state;


void PER_APP_Tick(void);
APP_Status PER_APP_Init_BLE(void);
APP_Status PER_APP_Service_Init(void);
APP_Status PER_APP_Advertise(void);
void PER_APP_remote_command_parser( uint8_t *data, uint8_t length );
void PER_APP_Disconnect( uint8_t reason );
void PER_APP_Error_Handler(void);
void PER_APP_send_OnOff_stream_command( uint8_t command );
void PER_APP_requested_config_response( uint8_t *data );

#endif //ROLE == PERIPHERAL

#endif /* __PERIPHERAL_MNGR_APP_H */


