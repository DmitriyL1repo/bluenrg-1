#include "common_conf.h"
#include "central_mngr_app.h"
#include "peripheral_mngr_app.h"
#include "BlueNRG1.h"
#include "BlueNRG1_conf.h"
#include "bluenrg1_hal.h"
#include "bluenrg1_gap.h"
#include "bluenrg1_gatt_server.h"
#include "ble_status.h"
#include "sm.h"
#include "clock.h"
#include "HDM_profile.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>

//Service UUID
// 21fe5d02-6400-4fc2-906d-becccb2008eb
extern const uint8_t hdm_service_uuid[16];
//Control point char UUID
// 21fe5d02-6401-4fc2-906d-becccb2008eb
extern const uint8_t hdm_char_cp_uuid[16];
//Status char UUID
// 21fe5d02-6402-4fc2-906d-becccb2008eb
extern const uint8_t hdm_char_status_uuid[16];
//Stream char UUID
// 21fe5d02-6403-4fc2-906d-becccb2008eb
extern const uint8_t hdm_char_stream_uuid[16];

#if ROLE == CENTRAL

enum
{
  HDMP_STATE_IDLE = 0,
  HDMP_STATE_SRV_DISCVRD,
  HDMP_STATE_CHAR_CP_DISCVRD,
  HDMP_STATE_CHAR_STATUS_DISCVRD,
  HDMP_STATE_CHAR_STREAM_DISCVRD,
  HDMP_STATE_CHAR_STAT_SUBSCRBD  
};

typedef struct
{
  uint16_t service;
  uint16_t char_cp;
  uint16_t char_status;
  uint16_t char_stream;
  uint16_t end_group;
}HDM_handles_type;

typedef union
{
  uint16_t byte;
  struct
  {
    uint16_t serv_disc_req_sent          :1;
    uint16_t char_cp_disc_req_sent       :1;
    uint16_t char_st_disc_req_sent       :1;
    uint16_t char_str_disc_req_sent      :1;
    uint16_t char_st_subscr_req_sent     :1;
    uint16_t stream_active               :1;
    uint16_t status                      :4;
    uint16_t rfu                         :6;
  }bits;
}HDMP_flags_type;

//------------------------------------------------------------------------------

const uint8_t notification_enable[2] = {0x01, 0x00};
const uint8_t notification_disable[2] = {0x00, 0x00};
static HDM_handles_type hdmp_handles;
volatile HDMP_flags_type hdmp_flags;

#ifdef THROUGHPUT_TEST_ENABLE
static uint16_t pn_prev = 0;
static uint16_t pn_cur = 0;
static uint16_t lost_packets = 0;
#endif
//------ Private functions------------------------------------------------------

void clear_handles(void);
void hdmp_discover_service(void);
void hdmp_discover_char_cp(void);
void hdmp_discover_char_status(void);
void hdmp_discover_char_stream(void);
void hdmp_subscribe_on_status_char(void);

#ifdef THROUGHPUT_TEST_ENABLE
void ThroughputDemoHandler( uint8_t *data, uint8_t size );
void ThroughputDemoProcess(void);
#endif
//------------------------------------------------------------------------------

void HDM_profile_process(void)
{
  switch( hdmp_flags.bits.status )
  {
  case HDMP_STATE_IDLE:
    hdmp_discover_service();
    break;
  case HDMP_STATE_SRV_DISCVRD:
    hdmp_discover_char_cp();
    break;
  case HDMP_STATE_CHAR_CP_DISCVRD:
    hdmp_discover_char_status();
    break;
  case HDMP_STATE_CHAR_STATUS_DISCVRD:
    hdmp_discover_char_stream();
    break;
  case HDMP_STATE_CHAR_STREAM_DISCVRD:
    hdmp_subscribe_on_status_char();
    break; 
  case HDMP_STATE_CHAR_STAT_SUBSCRBD:
    //Do nothing. Profile ready to work.
#ifdef THROUGHPUT_TEST_ENABLE
    ThroughputDemoProcess();
#endif    
    break;
  }
}

//------------------------------------------------------------------------------

void HDM_profile_reset(void)
{  
  hdmp_flags.byte = 0;
  memset( (void*)&hdmp_handles, 0, sizeof(HDM_handles_type) );
}

//------------------------------------------------------------------------------

void HDM_profile_OnOff_stream( uint8_t state )
{
  tBleStatus ret;
  uint8_t data[2];
  
  if( state )
    memcpy( data, notification_enable, 2 );
  else
    memcpy( data, notification_disable, 2 );
  
  ret = aci_gatt_write_char_desc(CENTRAL_APP_Get_Connection_Handle(),
                                 hdmp_handles.char_stream + 2, 2, data);
  
  if( ret == BLE_STATUS_SUCCESS )
    hdmp_flags.bits.stream_active = state; // It's better to move this code to gatt_cmplt_evt handler
  
}

//------------------------------------------------------------------------------

void HDM_profile_write_remote_config( uint8_t *data, uint8_t length )
{
  tBleStatus ret;
  uint8_t data_buf[17];
  
  if( length != 32 ) return;
  
  data_buf[0] = RCOM_SET_CONF_PART1;
  memcpy( &data_buf[1], &data[0], 16 );  
  ret = aci_gatt_write_without_resp(CENTRAL_APP_Get_Connection_Handle(),
                                 hdmp_handles.char_cp + 1, 17, data_buf);
  if( ret )
  {
    PRINT_DEBUG_1("Set CP val1 err: 0x%02X\n", ret);
    return;
  }
  
  data_buf[0] = RCOM_SET_CONF_PART2;
  memcpy( &data_buf[1], &data[16], 16 );  
  ret = aci_gatt_write_without_resp(CENTRAL_APP_Get_Connection_Handle(),
                                 hdmp_handles.char_cp + 1, 17, data_buf);
  if( ret )
  {
    PRINT_DEBUG_1("Set CP val2 err: 0x%02X\n", ret);
    return;
  }    
}

//------------------------------------------------------------------------------

void HDM_profile_read_remote_config(void)
{
  tBleStatus ret;
  uint8_t data_buf = RCOM_GET_CONF;
  
  ret = aci_gatt_write_without_resp(CENTRAL_APP_Get_Connection_Handle(),
                                 hdmp_handles.char_cp + 1, 1, &data_buf);
  if( ret )
  {
    PRINT_DEBUG_1("Set CP rcom get err: 0x%02X\n", ret);    
  }      
}

//------------------------------------------------------------------------------

uint8_t HDM_profile_is_stream_active( void )
{
  return hdmp_flags.bits.stream_active;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

void hdmp_discover_service(void)
{
  tBleStatus ret;
  UUID_t serv_uuid;
  
  if( hdmp_flags.bits.serv_disc_req_sent ) return;
  
  memcpy( serv_uuid.UUID_128, hdm_service_uuid, 16 ); 
    
  ret =  aci_gatt_disc_primary_service_by_uuid( CENTRAL_APP_Get_Connection_Handle(),
                                                UUID_TYPE_128, &serv_uuid );
  if( ret )
  {
    PRINT_DEBUG_1("Serv disc err: 0x%02X\n", ret);
  }
  else hdmp_flags.bits.serv_disc_req_sent = 1;

}

//------------------------------------------------------------------------------

void hdmp_discover_char_cp(void)
{
  tBleStatus ret;
  UUID_t char_uuid;
  
  if( hdmp_flags.bits.char_cp_disc_req_sent ) return;

  memcpy( char_uuid.UUID_128, hdm_char_cp_uuid, 16 );

  ret = aci_gatt_disc_char_by_uuid( CENTRAL_APP_Get_Connection_Handle(),
                                    hdmp_handles.service, hdmp_handles.end_group,
                                    UUID_TYPE_128, &char_uuid );
    
  if( ret )
  {
    PRINT_DEBUG_1("Char cp disc err: 0x%02X\n", ret);
  }
  else hdmp_flags.bits.char_cp_disc_req_sent = 1;
}

//------------------------------------------------------------------------------

void hdmp_discover_char_status(void)
{
  tBleStatus ret;
  UUID_t char_uuid;
  
  if( hdmp_flags.bits.char_st_disc_req_sent ) return;

  memcpy( char_uuid.UUID_128, hdm_char_status_uuid, 16 );

  ret = aci_gatt_disc_char_by_uuid( CENTRAL_APP_Get_Connection_Handle(),
                                    hdmp_handles.service, hdmp_handles.end_group,
                                    UUID_TYPE_128, &char_uuid );
    
  if( ret )
  {
    PRINT_DEBUG_1("Char st disc err: 0x%02X\n", ret);
  }
  else hdmp_flags.bits.char_st_disc_req_sent = 1;  
}

//------------------------------------------------------------------------------

void hdmp_discover_char_stream(void)
{
  tBleStatus ret;
  UUID_t char_uuid;  
  
  if( hdmp_flags.bits.char_str_disc_req_sent ) return;

  memcpy( char_uuid.UUID_128, hdm_char_stream_uuid, 16 );

  ret = aci_gatt_disc_char_by_uuid( CENTRAL_APP_Get_Connection_Handle(),
                                    hdmp_handles.service, hdmp_handles.end_group,
                                    UUID_TYPE_128, &char_uuid );
    
  if( ret )
  {
    PRINT_DEBUG_1("Char str disc err: 0x%02X\n", ret);
  }
  else hdmp_flags.bits.char_str_disc_req_sent = 1;    
}

//------------------------------------------------------------------------------

void hdmp_subscribe_on_status_char(void)
{
  tBleStatus ret;
  
  if( hdmp_flags.bits.char_st_subscr_req_sent ) return;
  
  ret = aci_gatt_write_char_desc(CENTRAL_APP_Get_Connection_Handle(),
                                 hdmp_handles.char_status + 2,
                                 2,
                                 (uint8_t*)notification_enable );
  
  if( ret == BLE_STATUS_SUCCESS ) 
    hdmp_flags.bits.char_st_subscr_req_sent = 1;
  
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

void aci_att_find_by_type_value_resp_event(uint16_t Connection_Handle,
                                           uint8_t Num_of_Handle_Pair,
                                           Attribute_Group_Handle_Pair_t Attribute_Group_Handle_Pair[])
{
  uint8_t i;
  
  for( i = 0; i < Num_of_Handle_Pair; i++ )
  {    
    hdmp_handles.service = Attribute_Group_Handle_Pair[i].Found_Attribute_Handle;
    hdmp_handles.end_group = Attribute_Group_Handle_Pair[i].Group_End_Handle;
  }

}

//------------------------------------------------------------------------------

void aci_gatt_error_resp_event(uint16_t Connection_Handle,
                               uint8_t Req_Opcode,
                               uint16_t Attribute_Handle,
                               uint8_t Error_Code)
{    
  if( Error_Code != 0x0A )
  {
    PRINT_DEBUG_1("GATT err 0x%02X ", Error_Code );
    PRINT_DEBUG_2("on opcode 0x%02X at handle 0x%04X\n", Req_Opcode, Attribute_Handle );
  }     
}

//------------------------------------------------------------------------------

void aci_gatt_disc_read_char_by_uuid_resp_event(uint16_t Connection_Handle,
                                                uint16_t Attribute_Handle,
                                                uint8_t Attribute_Value_Length,
                                                uint8_t Attribute_Value[])
{
  PRINT_DEBUG_1("Attr handle 0x%04X ", Attribute_Handle );
  PRINT_DEBUG_1("len: %u\n", Attribute_Value_Length );
  if( hdmp_flags.bits.char_cp_disc_req_sent )
  {
    hdmp_handles.char_cp = Attribute_Handle;
  }
  else if( hdmp_flags.bits.char_st_disc_req_sent )
  {
    hdmp_handles.char_status = Attribute_Handle;
  }
  else if( hdmp_flags.bits.char_str_disc_req_sent )
  {
    hdmp_handles.char_stream = Attribute_Handle;
  }
}

//------------------------------------------------------------------------------

void aci_gatt_proc_complete_event(uint16_t Connection_Handle,
                                  uint8_t Error_Code)
{  
  if( Error_Code )
  {  
    PRINT_DEBUG_1("GATT proc compl 0x%02X\n", Error_Code );
    return;
  }
  
  if( hdmp_flags.bits.serv_disc_req_sent )
  {
    hdmp_flags.bits.serv_disc_req_sent = 0;
    hdmp_flags.bits.status = HDMP_STATE_SRV_DISCVRD;
  }
  else if( hdmp_flags.bits.char_cp_disc_req_sent )
  {
    hdmp_flags.bits.char_cp_disc_req_sent = 0;
    hdmp_flags.bits.status = HDMP_STATE_CHAR_CP_DISCVRD;
  }
  else if( hdmp_flags.bits.char_st_disc_req_sent )
  {
    hdmp_flags.bits.char_st_disc_req_sent = 0;
    hdmp_flags.bits.status = HDMP_STATE_CHAR_STATUS_DISCVRD;
  }
  else if( hdmp_flags.bits.char_str_disc_req_sent )
  {
    hdmp_flags.bits.char_str_disc_req_sent = 0;
    hdmp_flags.bits.status = HDMP_STATE_CHAR_STREAM_DISCVRD;    
  }
  else if( hdmp_flags.bits.char_st_subscr_req_sent )
  {
    hdmp_flags.bits.char_st_subscr_req_sent = 0;
    hdmp_flags.bits.status = HDMP_STATE_CHAR_STAT_SUBSCRBD;
    PRINT_DEBUG_0("Profile READY!\n");
  }
  else
  {
  
  }
  
}

//------------------------------------------------------------------------------
#ifdef THROUGHPUT_TEST_ENABLE

void ThroughputDemoHandler( uint8_t *data, uint8_t size )
{  
  static uint8_t prev_data = 0xFF;
  pn_cur++; 
  
  if( (data[0] - 1) != prev_data )
    lost_packets++;
  
  prev_data = data[0];
  //PRINT_DEBUG_1("%02X ", data[0] );    
  /*
  for( i = 0; i < size; i++ )
  {
    PRINT_DEBUG_1("%02X ", data[i] );    
  }
  PRINT_DEBUG_0("\n");    
  */
}

//------------------------------------------------------------------------------

void ThroughputDemoProcess(void)
{  
  static uint32_t timer = 0; 
  
  if( (Clock_Time() >= timer) && hdmp_flags.bits.stream_active )
  {
    PRINT_DEBUG_2("%u pcts in %u ms ", (pn_cur - pn_prev), (1000 +(Clock_Time() - timer)) );
    PRINT_DEBUG_1("Lost %u\n", lost_packets);
    //PRINT_DEBUG_1("Sz: %u\n", size);
    
    lost_packets = 0;
    pn_prev = pn_cur;
    timer = Clock_Time() + 1000;
    
  }  
  
}

#endif
//------------------------------------------------------------------------------

void aci_gatt_notification_event(uint16_t Connection_Handle,
                                 uint16_t Attribute_Handle,
                                 uint8_t Attribute_Value_Length,
                                 uint8_t Attribute_Value[])
{
  if( Attribute_Handle == hdmp_handles.char_stream + 1 )
  {    
#ifdef THROUGHPUT_TEST_ENABLE
    ThroughputDemoHandler( Attribute_Value, Attribute_Value_Length );
#else    
    CM_fill_stream_buffer( Attribute_Value_Length, Attribute_Value );
#endif 
    return;
  }
  
  if( Attribute_Handle == hdmp_handles.char_status + 1 )
  {
    CENTRAL_APP_remote_command_parser( Attribute_Value, Attribute_Value_Length );
  }
}


#endif