#include "common_conf.h"
#include "central_mngr_app.h"
#include "peripheral_mngr_app.h"
#include "BlueNRG1.h"
#include "BlueNRG1_conf.h"
#include "bluenrg1_hal.h"
#include "bluenrg1_gap.h"
#include "bluenrg1_gatt_server.h"
#include "ble_status.h"
#include "sm.h"
#include "clock.h"
#include "HDM_service.h"
#include "comm_mngr.h"
#include "peripheral_mngr_app.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>

//Service UUID
// 21fe5d02-6400-4fc2-906d-becccb2008eb
const uint8_t hdm_service_uuid[16] = 
{0x21,0xfe,0x5d,0x02,0x64,0x00,0x4f,0xc2,0x90,0x6d,0xbe,0xcc,0xcb,0x20,0x08,0xeb};
//Control point char UUID
// 21fe5d02-6401-4fc2-906d-becccb2008eb
const uint8_t hdm_char_cp_uuid[16] = 
{0x21,0xfe,0x5d,0x02,0x64,0x01,0x4f,0xc2,0x90,0x6d,0xbe,0xcc,0xcb,0x20,0x08,0xeb};
//Status char UUID
// 21fe5d02-6402-4fc2-906d-becccb2008eb
const uint8_t hdm_char_status_uuid[16] = 
{0x21,0xfe,0x5d,0x02,0x64,0x02,0x4f,0xc2,0x90,0x6d,0xbe,0xcc,0xcb,0x20,0x08,0xeb};
//Stream char UUID
// 21fe5d02-6403-4fc2-906d-becccb2008eb
const uint8_t hdm_char_stream_uuid[16] = 
{0x21,0xfe,0x5d,0x02,0x64,0x03,0x4f,0xc2,0x90,0x6d,0xbe,0xcc,0xcb,0x20,0x08,0xeb};

#if ROLE == PERIPHERAL

#define HDM_CP_CHAR_SIZE        20
#define HDM_STAT_CHAR_SIZE      20
#define HDM_STREAM_CHAR_SIZE    20

typedef struct
{
  uint16_t service;
  uint16_t char_cp;
  uint16_t char_status;
  uint16_t char_stream;
}HDM_handles_type;

typedef union
{
  uint8_t byte;
  struct
  {
    uint8_t service_inited      :1;
    uint8_t stream_active       :1;
    uint8_t status_active       :1;
    uint8_t stream_buf_full     :1;
  }bits;
}HDM_flags_type;

//------------------------------------------------------------------------------

static HDM_handles_type hdm_handles;
static HDM_flags_type hdm_flags;

//------ Private functions------------------------------------------------------

void demo_throughput_process(void);
void input_stream_process(void);

//------------------------------------------------------------------------------

void HDM_service_init(void)
{
  tBleStatus ret;
  Service_UUID_t s_uuid;
  Char_UUID_t    c_uuid;
  
  hdm_flags.byte = 0;
  
  memcpy( s_uuid.Service_UUID_128, hdm_service_uuid, 16 );
    
  ret = aci_gatt_add_service( UUID_TYPE_128, &s_uuid,
                              PRIMARY_SERVICE, 10, &hdm_handles.service );
  
  if( ret )
  {
    PRINT_DEBUG_1("Serv add error: 0x%X\n", ret);
    return;
  }
  PRINT_DEBUG_1("Serv handle: 0x%04X\n", hdm_handles.service);
  
  //Add CP char
  memcpy( c_uuid.Char_UUID_128, hdm_char_cp_uuid, 16 );
  
  ret = aci_gatt_add_char( hdm_handles.service, UUID_TYPE_128, &c_uuid,
                           HDM_CP_CHAR_SIZE, CHAR_PROP_WRITE_WITHOUT_RESP,
                           ATTR_PERMISSION_NONE, GATT_NOTIFY_ATTRIBUTE_WRITE,
                           MIN_ENCRY_KEY_SIZE, CHAR_VALUE_LEN_VARIABLE,
                           &hdm_handles.char_cp );
  if( ret )
  {
    PRINT_DEBUG_1("Char CP add error: 0x%X\n", ret);
    return;
  }
  PRINT_DEBUG_1("Char CP handle: 0x%04X\n", hdm_handles.char_cp);
  
  //Add status char
  memcpy( c_uuid.Char_UUID_128, hdm_char_status_uuid, 16 );
  
  ret = aci_gatt_add_char( hdm_handles.service, UUID_TYPE_128, &c_uuid,
                           HDM_STAT_CHAR_SIZE, CHAR_PROP_READ | CHAR_PROP_NOTIFY,
                           ATTR_PERMISSION_NONE, GATT_DONT_NOTIFY_EVENTS,
                           MIN_ENCRY_KEY_SIZE, CHAR_VALUE_LEN_VARIABLE,
                           &hdm_handles.char_status );
  if( ret )
  {
    PRINT_DEBUG_1("Char ST add error: 0x%X\n", ret);
    return;
  }
  PRINT_DEBUG_1("Char ST handle: 0x%04X\n", hdm_handles.char_status);
  
  //Add stream char
  memcpy( c_uuid.Char_UUID_128, hdm_char_stream_uuid, 16 );
  
  ret = aci_gatt_add_char( hdm_handles.service, UUID_TYPE_128, &c_uuid,
                           HDM_STREAM_CHAR_SIZE, CHAR_PROP_NOTIFY,
                           ATTR_PERMISSION_NONE, GATT_DONT_NOTIFY_EVENTS,
                           MIN_ENCRY_KEY_SIZE, CHAR_VALUE_LEN_VARIABLE,
                           &hdm_handles.char_stream );
  if( ret )
  {
    PRINT_DEBUG_1("Char STR add error: 0x%X\n", ret);
    return;
  }
  PRINT_DEBUG_1("Char STR handle: 0x%04X\n", hdm_handles.char_stream); 
  
  hdm_flags.bits.service_inited = 1;
}

//------------------------------------------------------------------------------

void HDM_service_process(void)
{
  if( ! hdm_flags.bits.service_inited ) return;
  
  if( hdm_flags.bits.stream_active )
#ifdef THROUGHPUT_TEST_ENABLE    
    demo_throughput_process();
#else
    input_stream_process();
#endif  
  
}

//------------------------------------------------------------------------------

void HDM_service_reset(void)
{  
  if( hdm_flags.bits.stream_active )
    PER_APP_send_OnOff_stream_command(0);
  
  if( hdm_flags.bits.service_inited )
  {
    hdm_flags.byte = 0;
    hdm_flags.bits.service_inited = 1;
  }
  else
  {
    HDM_service_init();
  }  
}

//------------------------------------------------------------------------------

void aci_gatt_attribute_modified_event(uint16_t Connection_Handle,
                                       uint16_t Attr_Handle,
                                       uint16_t Offset,
                                       uint8_t Attr_Data_Length,
                                       uint8_t Attr_Data[])
{
  //-------------
  uint8_t i;
  PRINT_DEBUG_2("Att modified. Handle 0x%04X, offset %u\n", Attr_Handle, Offset );
  for( i = 0; i < Attr_Data_Length; i++ )
  {
    PRINT_DEBUG_1("%02X ", Attr_Data[i] );
  }
  PRINT_DEBUG_0("\n");
    
  //---------------  
  
  if( Attr_Handle == hdm_handles.char_stream + 2 )
  {    
    hdm_flags.bits.stream_active = Attr_Data[0];
    PER_APP_send_OnOff_stream_command( Attr_Data[0] );
  }
  
  if( Attr_Handle == hdm_handles.char_status + 2 )
  {    
    hdm_flags.bits.status_active = Attr_Data[0];
  }
  
  if( Attr_Handle == hdm_handles.char_cp + 1 )
  {
    PER_APP_remote_command_parser( Attr_Data, Attr_Data_Length );
  }  
 
}
                      
//------------------------------------------------------------------------------

void aci_gatt_tx_pool_available_event(uint16_t Connection_Handle,
                                      uint16_t Available_Buffers)
{       
  /* It allows to notify when at least 2 GATT TX buffers are available */  
  hdm_flags.bits.stream_buf_full = 0;  
} 

//------------------------------------------------------------------------------

void demo_throughput_process(void)
{
  static uint16_t pn = 0;
  tBleStatus ret;  
  uint8_t buffer[HDM_STREAM_CHAR_SIZE];
  
  if( ! hdm_flags.bits.stream_buf_full )
  {
    memset( (void*)buffer, pn, HDM_STREAM_CHAR_SIZE );    
       
    ret = aci_gatt_update_char_value( hdm_handles.service,
                                     hdm_handles.char_stream,
                                     0,
                                     HDM_STREAM_CHAR_SIZE,
                                     buffer );    
    
    
    if( ret == BLE_STATUS_INSUFFICIENT_RESOURCES )    
      hdm_flags.bits.stream_buf_full = 1;
    
    if( ret == BLE_STATUS_SUCCESS)
      pn++;
      
  }  
}

//------------------------------------------------------------------------------

void input_stream_process(void)
{
  tBleStatus ret;  
  uint8_t buffer[HDM_STREAM_CHAR_SIZE];
  
  if( ! hdm_flags.bits.stream_buf_full )
  {
    if( CM_fill_stream_buffer_for_BLE( HDM_STREAM_CHAR_SIZE, buffer) )
    {
      ret = aci_gatt_update_char_value( hdm_handles.service,
                                       hdm_handles.char_stream,
                                       0,
                                       HDM_STREAM_CHAR_SIZE,
                                       buffer );    
      
      
      if( ret == BLE_STATUS_INSUFFICIENT_RESOURCES )
        hdm_flags.bits.stream_buf_full = 1;
    }   
  }  
}

//------------------------------------------------------------------------------

uint8_t HDM_service_is_stream_active(void)
{
  return hdm_flags.bits.stream_active;
}

//------------------------------------------------------------------------------
//Retval
//0 - success
//1 - data size more than characteristic can take
//2 - radio busy
//3 - client is not subscribed on notification

uint8_t HDM_service_send_status_data( uint8_t *data, uint8_t size )
{
  tBleStatus ret;  

  if( size > HDM_STAT_CHAR_SIZE ) return 1;
  
  ret = aci_gatt_update_char_value( hdm_handles.service,
                                   hdm_handles.char_status,
                                   0, size, data );    
  
  
  if( ret == BLE_STATUS_INSUFFICIENT_RESOURCES )
    return 2;
  
  if( !hdm_flags.bits.status_active )
    return 3;
  
  return 0;    
}

//------------------------------------------------------------------------------


#endif
