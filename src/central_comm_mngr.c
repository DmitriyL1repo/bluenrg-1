#include "common_conf.h"
#include "BlueNRG1.h"
#include "BlueNRG1_sysCtrl.h"
#include "ble_status.h"
#include "misc.h"
#include "clock.h"
#include "comm_mngr.h"
#include "uart_mngr.h"
#include "central_mngr_app.h"
#include "HDM_profile.h"

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#if ROLE == CENTRAL

#define COMMAND_HEADER_SIZE      2
#define COMMAND_DELIMITER        '$'
#define COMMAND_TIMEOUT_MS       100

enum
{
  CM_DUMMY_L = 0,
  CM_SCAN_PEERS,
  CM_CONNECT_TO_PEER,
  CM_DISCONNECT_PEER,
  CM_READ_REMOTE_CONFIG,
  CM_WRITE_REMOTE_CONFIG,
  CM_START_MONITOR,
  CM_STOP_MONITOR,
  CM_GET_CONN_STATUS,
  CM_DUMMY_H
};

#pragma pack(push, 1)
typedef struct
{
  uint8_t cm_ready_flag;
  uint8_t pWrite;
  uint8_t data[34];
}CM_rx_buffer_type;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct
{    
  uint8_t overflow_flag;
  uint16_t pWrite;
  uint16_t pRead;
  uint8_t data[STREAM_BUFFER_SIZE];
}CM_tx_buffer_type;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct
{
  uint8_t delimiter;
  uint8_t command_code;
  uint8_t payload[32];
}CM_command_type;
#pragma pack(pop)


//------ Private variables -------------------------------------------------

const uint8_t command_payload_size[10] = { 0xff, 0, 1, 0, 0, 32, 0, 0, 0, 0xff };
CM_rx_buffer_type cm_rx_buffer;
CM_tx_buffer_type cm_tx_buffer;

//------ Private functions -------------------------------------------------

uint8_t CM_get_payload_size(uint8_t command);
void CM_command_parser( uint8_t *data );
void CM_flush_stream_buffer(void);
void put_byte_to_buffer( uint8_t data );
uint16_t get_available_data_size(void);
void CM_reset_tx_buffer(void);
uint8_t CM_get_scan_peers_com_code(void);

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------

void CM_init(void)
{
  CM_reset_rx_buffer();
  CM_reset_tx_buffer();
  init_uart(); 
}

//--------------------------------------------------------------------------

void CM_uart_rx_callback( uint8_t data )
{  
  if( cm_rx_buffer.pWrite == 0 )
  {
    if( data == COMMAND_DELIMITER )
    {
      cm_rx_buffer.data[cm_rx_buffer.pWrite] = data;
      cm_rx_buffer.pWrite++;
    }
  }
  else
  {
    if( cm_rx_buffer.cm_ready_flag ) return;
    
    cm_rx_buffer.data[cm_rx_buffer.pWrite] = data;
    cm_rx_buffer.pWrite++;
    if( cm_rx_buffer.pWrite >= COMMAND_HEADER_SIZE )
    {
      if( cm_rx_buffer.pWrite == ( CM_get_payload_size( cm_rx_buffer.data[1] ) +  COMMAND_HEADER_SIZE) )      
        cm_rx_buffer.cm_ready_flag = 1;             
    }
  }
}

//--------------------------------------------------------------------------

void CM_command_monitor(void)
{
  /*
  static uint32_t wd_timer = 0;
  
  if( cm_rx_buffer.pWrite != 0 )
  {
    if( wd_timer == 0 )
      wd_timer = Clock_Time() + COMMAND_TIMEOUT_MS;
    
    if( wd_timer < Clock_Time() )
    {
      CM_reset_rx_buffer();
      wd_timer = 0;
    }
  }
  */
  if( cm_rx_buffer.cm_ready_flag )
  {
    CM_command_parser( cm_rx_buffer.data );
    CM_reset_rx_buffer();    
  }
  
  if( HDM_profile_is_stream_active() )
  {
    CM_flush_stream_buffer();
  }
}

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------

void CM_command_parser( uint8_t *data )
{
  CM_command_type *command = (void*)data;
  
  switch( command->command_code )
  {
  case CM_SCAN_PEERS:
    CENTRAL_APP_Scan();
    break;
  case CM_CONNECT_TO_PEER:
    CENTRAL_APP_Connect_To_Peer( command->payload[0] );
    break;
  case CM_DISCONNECT_PEER:
    CENTRAL_APP_Disconnect( ERR_RMT_USR_TERM_CONN );
    break;
  case CM_READ_REMOTE_CONFIG:
    HDM_profile_read_remote_config();
    break;
  case CM_WRITE_REMOTE_CONFIG:
    HDM_profile_write_remote_config( command->payload, 32 );    
    break;
  case CM_START_MONITOR:
    HDM_profile_OnOff_stream(1);
    break;
  case CM_STOP_MONITOR:
    HDM_profile_OnOff_stream(0);
    break;
  case CM_GET_CONN_STATUS:
    {
      uint8_t response[2];      
      response[0] = CM_GET_CONN_STATUS;
      response[1] = CENTRAL_APP_Get_Connection_Status();
      CM_uart_tx_command( response, 2 );
    }
    break;
  }  
}

//--------------------------------------------------------------------------

uint8_t CM_get_scan_peers_com_code(void)
{
  return CM_SCAN_PEERS;
}

//--------------------------------------------------------------------------

uint8_t CM_get_payload_size(uint8_t command)
{
  if( (command > CM_DUMMY_L) && (command < CM_DUMMY_H) )  
    return command_payload_size[command];  
  else
    return 0;
}

//--------------------------------------------------------------------------

void CM_reset_rx_buffer(void)
{
  memset( (void*)&cm_rx_buffer, 0, sizeof(CM_rx_buffer_type) );
}

//--------------------------------------------------------------------------

void CM_reset_tx_buffer(void)
{
  memset( (void*)&cm_tx_buffer, 0, sizeof(CM_tx_buffer_type) );
}

//--------------------------------------------------------------------------

void CM_uart_tx_command( uint8_t *data, uint8_t length )
{
  uint8_t i;
  
  if( HDM_profile_is_stream_active() ) return;
  
  putchar(COMMAND_DELIMITER);
  for( i = 0; i < length; i++ )
  {
    putchar(data[i]);
  }
}

//--------------------------------------------------------------------------

void CM_flush_stream_buffer(void)
{
  uint16_t i;

  if( cm_tx_buffer.pRead == cm_tx_buffer.pWrite ) return;
  
  for( i = 0; i < 20; i++ )
  {
    putchar( cm_tx_buffer.data[cm_tx_buffer.pRead++] );
    if( cm_tx_buffer.pRead == STREAM_BUFFER_SIZE)
      cm_tx_buffer.pRead = 0;
    if( cm_tx_buffer.pRead == cm_tx_buffer.pWrite ) break;
  }  
}

//--------------------------------------------------------------------------

void CM_fill_stream_buffer( uint8_t size, uint8_t *data )
{
  uint16_t real_size, avail_size, i;
  
  avail_size = get_available_data_size();
  real_size = (uint16_t)size > avail_size ? avail_size : size;
  
  for( i = 0; i < real_size; i++ )
  {
    put_byte_to_buffer( data[i] );
  }  
}

//--------------------------------------------------------------------------

void put_byte_to_buffer( uint8_t data )
{
  cm_tx_buffer.data[cm_tx_buffer.pWrite] = data;
  cm_tx_buffer.pWrite++;
  if( cm_tx_buffer.pWrite == STREAM_BUFFER_SIZE )
    cm_tx_buffer.pWrite = 0;
  if( cm_tx_buffer.pWrite == cm_tx_buffer.pRead )
  {
    cm_tx_buffer.overflow_flag = 1;
    cm_tx_buffer.pWrite--; //Decrement write pointer to avoid rd/wr pointers overlapping
  }
  
}

//--------------------------------------------------------------------------

uint16_t get_available_data_size( void )
{
  uint16_t size;
  
  __disable_interrupt();
  if( cm_tx_buffer.pRead < cm_tx_buffer.pWrite )  
    size = cm_tx_buffer.pWrite - cm_tx_buffer.pRead;
  else if( cm_tx_buffer.pRead > cm_tx_buffer.pWrite )  
    size = cm_tx_buffer.pWrite + ( STREAM_BUFFER_SIZE - cm_tx_buffer.pRead);  
  else
  {
    if( cm_tx_buffer.overflow_flag  )
      size = 0;
    else
      size = sizeof(cm_tx_buffer.data);
  }
    
  __enable_interrupt();
  
  return size;  
}


#endif