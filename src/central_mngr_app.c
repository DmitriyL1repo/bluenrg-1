#include "common_conf.h"
#include "central_mngr_app.h"
#include "comm_mngr.h"
#include "peripheral_mngr_app.h"
#include "BlueNRG1.h"
#include "BlueNRG1_conf.h"
#include "bluenrg1_hal.h"
#include "bluenrg1_gap.h"
#include "bluenrg1_gatt_server.h"
#include "ble_status.h"
#include "sm.h"
#include "HDM_profile.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#if ROLE == CENTRAL

const char peer_name_template[] = {NAME_WEAR};
scannedDeviceInfoType periph_device_info[PERIPH_LIST_MAX_NUMBER];
uint8_t periph_device_info_cur_index;

uint8_t CENTRAL_BDADDR[] = { 0x56, 0x11, 0x07, 0x01, 0x16, 0xE2 }; 
volatile uint8_t APP_CENTRAL_state = APP_STATUS_IDLE;
volatile central_app_flags_type app_flags;
uint16_t conn_handle = 0xFFFF;

//------ Private functions---------------------------------------------------------------------------
void clear_scan_list(void);
scannedDeviceInfoType *get_free_cell_in_scan_list(void);
scannedDeviceInfoType *get_cell_by_index_in_scan_list( uint8_t index );
void copy_peer_name_from_adv_report( uint8_t *adv_report, uint8_t adv_report_size,  uint8_t *name );
uint8_t MAC_is_exist_in_storage( uint8_t *mac );
void print_MAC_address( uint8_t *mac );
void submit_scan_report(void);

extern void DelayMs(volatile uint32_t lTimeMs);
extern uint8_t CM_get_scan_peers_com_code(void);
//----------------------------------------------------------------------------------------------------

APP_Status CENTRAL_APP_Init_BLE(void)
{
  uint8_t ret = 0;
  uint16_t service_handle, dev_name_char_handle, appearance_char_handle;
  
  ret = aci_hal_write_config_data(CONFIG_DATA_PUBADDR_OFFSET, CONFIG_DATA_PUBADDR_LEN, CENTRAL_BDADDR);
  
  if (ret != BLE_STATUS_SUCCESS)
  {
    return APP_ERROR;
  }
  
  aci_hal_set_tx_power_level(1, 7);
  
  /* GATT Init */
  ret = aci_gatt_init();
  if (ret != BLE_STATUS_SUCCESS) 
  { 
    return APP_ERROR;
  }
 
  /* GAP Init */
  ret = aci_gap_init(GAP_CENTRAL_ROLE, 0, 0x07, &service_handle, &dev_name_char_handle, &appearance_char_handle);
  if (ret != BLE_STATUS_SUCCESS) 
  {
    return APP_ERROR;
  }

  /* Set auth requirement*/
  aci_gap_set_authentication_requirement(MITM_PROTECTION_REQUIRED, OOB_AUTH_DATA_ABSENT, NULL, 7, 16, USE_FIXED_PIN_FOR_PAIRING, 123456, BONDING);
        
  app_flags.byte = 0;
  
  return APP_SUCCESS;
}

//----------------------------------------------------------------------------------------------------

APP_Status CENTRAL_APP_Scan(void)
{
  uint8_t ret;
  
  PRINT_DEBUG_0("Start scan\n");
  if( APP_CENTRAL_state == APP_STATUS_SCAN )
  {
    PRINT_DEBUG_0("Device is scanning\n");
    return APP_SUCCESS;
  }
  clear_scan_list();
  
  ret = aci_gap_start_general_discovery_proc( SCAN_INTERVAL, SCAN_WINDOW, 1, 1 );
  if (ret != BLE_STATUS_SUCCESS) 
  { 
    APP_CENTRAL_state = APP_STATUS_IDLE;
    PRINT_DEBUG_1("Start scan error 0x%02X\n", ret);
    return APP_ERROR;
  }
  
  APP_CENTRAL_state = APP_STATUS_SCAN;
  return APP_SUCCESS;
}

//----------------------------------------------------------------------------------------------------

APP_Status CENTRAL_APP_Connect_To_Peer( uint8_t peer_index )
{
  scannedDeviceInfoType *p_device_info;
  tBleStatus ret;
  
  p_device_info = get_cell_by_index_in_scan_list( peer_index );
  if( p_device_info == NULL ) return APP_ERROR;
  
  if( APP_CENTRAL_state == APP_STATUS_SCAN )
  {
    ret = aci_gap_terminate_gap_proc( GAP_GENERAL_DISCOVERY_PROC );
    if( ret )
    {
      PRINT_DEBUG_1("Terminate gen disc err: 0x%X\n", ret );
      return APP_ERROR;
    }
  }   
  
  PRINT_DEBUG_1("Peer adr type: %u\n", p_device_info->Peer_Address_Type );
  for( ret = 0; ret < 6; ret++ )
  {
    PRINT_DEBUG_1("0x%02X ", p_device_info->Peer_Address[ret] );
  }
  PRINT_DEBUG_0("\n"); 
  
  ret = aci_gap_create_connection(SCAN_INTERVAL, SCAN_WINDOW, //0x10, 0x10,
                                  p_device_info->Peer_Address_Type,
                                  p_device_info->Peer_Address,
                                  OWN_ADDR_TYPE,
                                  0x6, //0x6c
                                  0xc, //0x6c
                                  CONN_LATENCY,
                                  0xc80,
                                  0x000c,
                                  0x0010 ); //0c
  
  if( ret )
  {
    PRINT_DEBUG_1("Conn start err: 0x%X\n", ret );
    return APP_ERROR;
  }
 
  return APP_SUCCESS;
}

//----------------------------------------------------------------------------------------------------

void CENTRAL_APP_Disconnect( uint8_t reason )
{
  tBleStatus ret;
  ret = aci_gap_terminate( conn_handle, reason );
  if( ret ) PRINT_DEBUG_1("Disconnect err: 0x%X\n", ret );  
}

//----------------------------------------------------------------------------------------------------

uint16_t CENTRAL_APP_Get_Connection_Handle(void)
{
  return conn_handle;
}

//----------------------------------------------------------------------------------------------------

uint8_t CENTRAL_APP_Get_Connection_Status(void)
{
  return APP_CENTRAL_state;
}

//----------------------------------------------------------------------------------------------------

void CENTRAL_APP_Tick(void)
{
  switch(APP_CENTRAL_state)
  {
  case APP_STATUS_IDLE:
  case APP_STATUS_SCAN:
    //Doing nothing. No user actions needed
    break;
  case APP_STATUS_SCAN_REPORT_RDY:    
    {      
      submit_scan_report();      
      APP_CENTRAL_state = APP_STATUS_IDLE;
    }
    break;
  case APP_STATUS_CONNECTED:    
    HDM_profile_process();
    break;
    
  }
}

//----------------------------------------------------------------------------------------------------

void CENTRAL_APP_remote_command_parser( uint8_t *data, uint8_t length )
{
  static uint8_t conf_data[33] = {0};
  
  if( length != 17 ) return;
  
  if( data[0] == RCOM_SET_CONF_PART1 )
    memcpy( conf_data, data, 17 );
  else if( data[0] == RCOM_SET_CONF_PART2 )
  {
    memcpy( &conf_data[17], &data[1], 16 );
    CM_uart_tx_command( conf_data, 33 );
    PRINT_DEBUG_2("D0:%u  D16:%u\n", conf_data[1], conf_data[17]);
    memset( conf_data, 0, 33 );
  }
  
  /*
  switch( data[0] )
  {
  case RCOM_SET_CONF_PART1:
    if( length == 17 ) CM_uart_tx_command( data, length );
    break;
  case RCOM_SET_CONF_PART2:
    if( length == 17 ) CM_uart_tx_command( data, length );
    break; 
  }
  */
}

//----------------------------------------------------------------------------------------------------
#define RESPONSE_SIZE ((PEER_NAME_MAX_LEN + 2) * PERIPH_LIST_MAX_NUMBER) + 1

void submit_scan_report(void)
{  
  uint8_t i = 1;      
  char response[RESPONSE_SIZE];
  scannedDeviceInfoType *peer;
  char *p;
  uint8_t size = 1;
  
  response[0] = CM_get_scan_peers_com_code();
  p = &response[1];
  
  
  
  //---------------------------
  do
  {
    peer = get_cell_by_index_in_scan_list(i);
    if( peer != NULL )        
    {
      PRINT_DEBUG_2("Dev %u: %s\n", i, peer->Peer_Name );
      sprintf( p, "#%u%s", i, peer->Peer_Name );
      size += strlen( (char*)peer->Peer_Name ) + 2;
      p += size;
    }
    i++;
  } while ( i <= PERIPH_LIST_MAX_NUMBER );     
  //------------------------ 
  
  CM_uart_tx_command( (uint8_t*)response, size );  
}

//----------------------------------------------------------------------------------------------------

void hci_le_advertising_report_event(uint8_t Num_Reports,
                                     Advertising_Report_t Advertising_Report[])
{
  uint8_t i;
  scannedDeviceInfoType *p_device_info;
  char peer_name[PEER_NAME_MAX_LEN];
      
  for( i = 0 ; i < Num_Reports; i++ )
  {
    p_device_info = get_free_cell_in_scan_list();
    
    if( (p_device_info != NULL) && (MAC_is_exist_in_storage( Advertising_Report[i].Address ) == 0 ) )
    {      
      memset( (void*)peer_name, 0, PEER_NAME_MAX_LEN );
      copy_peer_name_from_adv_report( Advertising_Report[i].Data, 
                                      Advertising_Report[i].Length_Data, 
                                      (uint8_t*)peer_name );
      
#if PEERS_FILTERING_BY_NAME_PREFIX_ENABLED      
      if( strncmp( peer_name, peer_name_template, PEER_NAME_PREFIX_SIZE ) == 0 )      
#else
      if( strlen( peer_name ) )
#endif        
      {
        p_device_info->index = periph_device_info_cur_index;
        periph_device_info_cur_index++;
        p_device_info->Peer_Address_Type = Advertising_Report[i].Address_Type;
        memcpy( p_device_info->Peer_Address, Advertising_Report[i].Address, 6 );
        memset( (void*)p_device_info->Peer_Name, 0, PEER_NAME_MAX_LEN );
        strcpy( (char*)p_device_info->Peer_Name, peer_name );
                
        print_MAC_address( p_device_info->Peer_Address );
        PRINT_DEBUG_2("Peer%u: %s\n", p_device_info->index, p_device_info->Peer_Name );
      }
    }    
  }
  
   
}

//----------------------------------------------------------------------------------------------------

void aci_gap_proc_complete_event(uint8_t Procedure_Code,
                                 uint8_t Status,
                                 uint8_t Data_Length,
                                 uint8_t Data[])
{
  if( Procedure_Code == GAP_GENERAL_DISCOVERY_PROC )
  {
    APP_CENTRAL_state = APP_STATUS_SCAN_REPORT_RDY;    
  }
  else if( Procedure_Code == GAP_DIRECT_CONNECTION_ESTABLISHMENT_PROC )
  {
    //Do nothing, just confirmation of successful connection establishment procedure.
  }
  else
  {
    PRINT_DEBUG_1( "Compl evt code: 0x%02X\n", Procedure_Code );
  }
}

//----------------------------------------------------------------------------------------------------

void hci_le_connection_complete_event(uint8_t Status,
                                      uint16_t Connection_Handle,
                                      uint8_t Role,
                                      uint8_t Peer_Address_Type,
                                      uint8_t Peer_Address[6],
                                      uint16_t Conn_Interval,
                                      uint16_t Conn_Latency,
                                      uint16_t Supervision_Timeout,
                                      uint8_t Master_Clock_Accuracy)
{
  if( Status == BLE_STATUS_SUCCESS )
  {
    //uint8_t ret;
        
    conn_handle = Connection_Handle;
    APP_CENTRAL_state = APP_STATUS_CONNECTED;
    HDM_profile_reset();
    
    //ret = aci_l2cap_connection_parameter_update_req(conn_handle,
    //                                                8 /* interval_min*/,
    //                                                8 /* interval_max */,
    //                                                0   /* slave_latency */,
    //                                                400 /*timeout_multiplier*/);
    
    //if( ret ) PRINT_DEBUG_1("Conn param upd req failed: 0x%02X\n", ret ); 
       
  }
  else
  {
    PRINT_DEBUG_1("Conn failed: 0x%02X\n", Status );
    CENTRAL_APP_Disconnect(ERR_RMT_USR_TERM_CONN);
  }
  
}

//-----------------------------------------------------------------------------------

void aci_l2cap_connection_update_req_event(uint16_t Connection_Handle,
                                           uint8_t Identifier,
                                           uint16_t L2CAP_Length,
                                           uint16_t Interval_Min,
                                           uint16_t Interval_Max,
                                           uint16_t Slave_Latency,
                                           uint16_t Timeout_Multiplier)
{
  //PRINT_DEBUG_1( "L2CAP_Length: %u\n", L2CAP_Length );
  PRINT_DEBUG_1( "Interval_Min: %u\n", Interval_Min );
  PRINT_DEBUG_1( "Interval_Max: %u\n", Interval_Max );
}

//----------------------------------------------------------------------------------------------------

void hci_disconnection_complete_event(uint8_t Status,
                                          uint16_t Connection_Handle,
                                          uint8_t Reason)
{
  APP_CENTRAL_state = APP_STATUS_IDLE;
  //DelayMs(100);         //Necessary for stack internal purposes as defined in API reference  
  PRINT_DEBUG_1( "Disconnected: 0x%02X\n", Reason );
}

//----------------------------------------------------------------------------------------------------

void aci_gatt_proc_timeout_event(uint16_t Connection_Handle)
{
  CENTRAL_APP_Disconnect(ERR_RMT_USR_TERM_CONN);
}

//----------------------------------------------------------------------------------------------------

void clear_scan_list(void)
{
  memset( (void*)periph_device_info, 0, sizeof(periph_device_info) );  
  periph_device_info_cur_index = 1;
}

//----------------------------------------------------------------------------------------------------

scannedDeviceInfoType *get_free_cell_in_scan_list(void)
{
  uint8_t i;
  
  for( i = 0; i < PERIPH_LIST_MAX_NUMBER; i++ )
  {
    if( periph_device_info[i].index == 0 ) return &periph_device_info[i];
  }
  return NULL;
}

//----------------------------------------------------------------------------------------------------

scannedDeviceInfoType *get_cell_by_index_in_scan_list( uint8_t index )
{ 
  uint8_t i;
  if( (index == 0) || (index > PERIPH_LIST_MAX_NUMBER) ) return NULL;
  
  for( i = 0; i < PERIPH_LIST_MAX_NUMBER; i++ )
  {
    if( periph_device_info[i].index == index ) return &periph_device_info[i];
  }
  
  return NULL;
}

//----------------------------------------------------------------------------------------------------

void copy_peer_name_from_adv_report( uint8_t *adv_report, uint8_t adv_report_size,  uint8_t *name )
{
  uint8_t field_size, field_type, i, name_size;
  uint8_t *p = adv_report; 
  
  do
  {    
    field_size = p[0];
    field_type = p[1];
    
    if( (field_type == AD_TYPE_SHORTENED_LOCAL_NAME) || (field_type == AD_TYPE_COMPLETE_LOCAL_NAME) )
    {
      name_size = (field_size - 1) > PEER_NAME_MAX_LEN ? PEER_NAME_MAX_LEN : (field_size - 1);      
      p++;
      p++;
      for( i = 0; i < name_size; i++ )
      {
        if( isalnum( p[i] ) ) name[i] = p[i];
        else name[i] = 0;          
      }     
      p = adv_report + adv_report_size;
    }
    else
    {
      p += (field_size + 1);
    }
    
  } while( (p - adv_report) <  adv_report_size );  
}

//----------------------------------------------------------------------------------------------------

uint8_t MAC_is_exist_in_storage( uint8_t *mac )
{
  uint8_t i;
  
  for( i = 0; i < PERIPH_LIST_MAX_NUMBER; i++ )
  {
    if( memcmp( mac, periph_device_info[i].Peer_Address, 6 ) == 0 ) return 1;    
  }
  return 0;
}

//----------------------------------------------------------------------------------------------------

void print_MAC_address( uint8_t *mac )
{
  uint8_t i;
  
  PRINT_DEBUG_0("MAC: ");
  for( i = 0; i < 6; i++ )
  {
    PRINT_DEBUG_1("0x%02X ", mac[i]);
  }
  PRINT_DEBUG_0("\n");
}

//----------------------------------------------------------------------------------------------------

#endif