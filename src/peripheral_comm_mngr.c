#include "common_conf.h"
#include "BlueNRG1.h"
#include "BlueNRG1_sysCtrl.h"
#include "misc.h"
#include "clock.h"
#include "comm_mngr.h"
#include "uart_mngr.h"
#include "central_mngr_app.h"
#include "HDM_service.h"

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#if ROLE == PERIPHERAL

#define COMMAND_HEADER_SIZE      2
#define COMMAND_DELIMITER        '$'
#define COMMAND_TIMEOUT_MS       100
#define RX_BUFFER_SIZE  (STREAM_BUFFER_SIZE + COMMAND_HEADER_SIZE)

enum
{
  CM_DUMMY_L = 0,
  CM_GET_CONF,  
  CM_DUMMY_H
};

#pragma pack(push, 1)
typedef struct
{  
  uint8_t cm_ready_flag;
  uint8_t overflow_flag;
  uint16_t pWrite;
  uint16_t pRead;
  uint8_t data[RX_BUFFER_SIZE];
}CM_rx_buffer_type;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct
{
  uint8_t delimiter;
  uint8_t command_code;
  uint8_t payload[32];
}CM_command_type;
#pragma pack(pop)

//------ Private variables -------------------------------------------------

const uint8_t command_payload_size[4] = { 0xff, 32, 0xff };
CM_rx_buffer_type cm_rx_buffer;

//------ Private functions -------------------------------------------------

void put_byte_to_buffer( uint8_t data );
uint8_t get_byte_from_buffer( void );
uint16_t get_available_data_size( void );
uint8_t CM_get_payload_size(uint8_t command);
void CM_command_parser( uint8_t *data );

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------

void CM_init(void)
{  
  CM_reset_rx_buffer();
  init_uart(); 
}

//--------------------------------------------------------------------------

void CM_uart_rx_callback( uint8_t data )
{
  if( HDM_service_is_stream_active() )
  {
    put_byte_to_buffer( data );
  }
  else
  {
    
    if( cm_rx_buffer.pWrite == 0 )
    {
      if( data == COMMAND_DELIMITER )
      {
        cm_rx_buffer.data[cm_rx_buffer.pWrite] = data;
        cm_rx_buffer.pWrite++;
      }
    }
    else
    {
      if( cm_rx_buffer.cm_ready_flag ) return;
      
      cm_rx_buffer.data[cm_rx_buffer.pWrite] = data;
      cm_rx_buffer.pWrite++;
      if( cm_rx_buffer.pWrite >= COMMAND_HEADER_SIZE )
      {
        if( cm_rx_buffer.pWrite == ( CM_get_payload_size( cm_rx_buffer.data[1] ) +  COMMAND_HEADER_SIZE) )
          cm_rx_buffer.cm_ready_flag = 1;
      }
    }
    
  }  
  
}

//--------------------------------------------------------------------------

void CM_uart_tx_command( uint8_t *data, uint8_t length )
{
  uint8_t i;
  putchar(COMMAND_DELIMITER);
  for( i = 0; i < length; i++ )
  {
    putchar(data[i]);
  }
}

//--------------------------------------------------------------------------

void CM_command_monitor(void)
{
  static uint32_t wd_timer = 0;
  
  if( ! HDM_service_is_stream_active() )
  {    
    if( cm_rx_buffer.pWrite != 0 )
    {
      if( wd_timer == 0 )
        wd_timer = Clock_Time() + COMMAND_TIMEOUT_MS;
      
      if( wd_timer < Clock_Time() )
      {
        CM_reset_rx_buffer();
        wd_timer = 0;
      }
    }
    
    if( cm_rx_buffer.cm_ready_flag )
    {
      CM_command_parser( cm_rx_buffer.data );
      CM_reset_rx_buffer();
    }
  }  
}

//--------------------------------------------------------------------------
//Retval
//0 - buffer hasn't been filled, not enough data
//1 - buffer filled

uint8_t CM_fill_stream_buffer_for_BLE( uint8_t required_size, uint8_t *data )
{
  uint8_t i;
  
  if( get_available_data_size() >= required_size )
  {
    for( i = 0; i < required_size; i++ )
    {
      data[i] = get_byte_from_buffer();
    }
    return 1;
  }
  else return 0;
}

//--------------------------------------------------------------------------

void CM_reset_rx_buffer(void)
{
  memset( (void*)&cm_rx_buffer, 0, sizeof(CM_rx_buffer_type) );
}

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------

void put_byte_to_buffer( uint8_t data )
{
  cm_rx_buffer.data[cm_rx_buffer.pWrite] = data;
  cm_rx_buffer.pWrite++;
  if( cm_rx_buffer.pWrite == RX_BUFFER_SIZE )
    cm_rx_buffer.pWrite = 0;
  if( cm_rx_buffer.pWrite == cm_rx_buffer.pRead )
  {
    cm_rx_buffer.overflow_flag = 1;
    cm_rx_buffer.pWrite--; //Decrement write pointer to avoid rd/wr pointers overlapping
  }
  
}

//--------------------------------------------------------------------------

uint8_t get_byte_from_buffer( void )
{  
  uint8_t data;
  //while( get_available_data_size() == 0);
  data = cm_rx_buffer.data[cm_rx_buffer.pRead++];
  if( cm_rx_buffer.pRead == RX_BUFFER_SIZE )
    cm_rx_buffer.pRead = 0;
    
  return data;
}

//--------------------------------------------------------------------------

uint16_t get_available_data_size( void )
{
  uint16_t size;
  
  __disable_interrupt();
  if( cm_rx_buffer.pRead < cm_rx_buffer.pWrite )  
    size = cm_rx_buffer.pWrite - cm_rx_buffer.pRead;
  else if( cm_rx_buffer.pRead > cm_rx_buffer.pWrite )  
    size = cm_rx_buffer.pWrite + ( RX_BUFFER_SIZE - cm_rx_buffer.pRead);  
  else
    size = 0;
  __enable_interrupt();
  
  return size;
  
}

//--------------------------------------------------------------------------

uint8_t CM_get_payload_size(uint8_t command)
{
  if( (command > CM_DUMMY_L) && (command < CM_DUMMY_H) )  
    return command_payload_size[command];  
  else
    return 0;
}

//--------------------------------------------------------------------------

void CM_command_parser( uint8_t *data )
{
  CM_command_type *command = (void*)data;
  
  switch( command->command_code )
  {
  case CM_GET_CONF:
    PER_APP_requested_config_response( command->payload );    
    break; 
  }  
}

//--------------------------------------------------------------------------

#endif