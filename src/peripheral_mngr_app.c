#include "BlueNRG1.h"
#include "BlueNRG1_conf.h"
#include "peripheral_mngr_app.h"
#include "bluenrg1_hal.h"
#include "bluenrg1_gap.h"
#include "bluenrg1_gatt_server.h"
#include "ble_status.h"
#include "sm.h"
#include "common_conf.h"
#include "HDM_service.h"
#include "comm_mngr.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
   
#if ROLE == PERIPHERAL


volatile uint16_t ServiceHandle;
volatile uint16_t conn_handle;
volatile uint8_t APP_PER_state = APP_STATUS_ADVERTISEMENT;

uint8_t PERIPHERAL_BDADDR[] = { 0x55, 0x11, 0x07, 0x01, 0x16, 0xE2 }; 

//------------------------------------------------------------------------------

extern void DelayMs(volatile uint32_t lTimeMs);
void PER_APP_config_update_handler( uint8_t *data, uint8_t length );

//------------------------------------------------------------------------------

APP_Status PER_APP_Init_BLE(void)
{
  uint8_t ret = 0;
  uint16_t service_handle, dev_name_char_handle, appearance_char_handle;
  
  ret = aci_hal_write_config_data(CONFIG_DATA_PUBADDR_OFFSET, CONFIG_DATA_PUBADDR_LEN, PERIPHERAL_BDADDR);
  
  if (ret != BLE_STATUS_SUCCESS)
  {
    return APP_ERROR;
  }
  
  aci_hal_set_tx_power_level(1, 7);
  
  /* GATT Init */
  ret = aci_gatt_init();
  if (ret != BLE_STATUS_SUCCESS) 
  { 
    return APP_ERROR;
  }
 
  /* GAP Init */
  ret = aci_gap_init(GAP_PERIPHERAL_ROLE, 0, 0x07, &service_handle, &dev_name_char_handle, &appearance_char_handle);
  if (ret != BLE_STATUS_SUCCESS) 
  {
    return APP_ERROR;
  }

  /* Set auth requirement*/
  aci_gap_set_authentication_requirement(MITM_PROTECTION_REQUIRED, OOB_AUTH_DATA_ABSENT, NULL, 7, 16, USE_FIXED_PIN_FOR_PAIRING, 123456, BONDING);
                               
  return APP_SUCCESS;
}

//------------------------------------------------------------------------------

APP_Status PER_APP_Advertise(void)
{
  uint8_t ret = 0;
  
  uint8_t local_name[] =
  {
    AD_TYPE_COMPLETE_LOCAL_NAME, NAME_WEAR
  };

  uint8_t manuf_data[23] = 
  {
    2,0x0A,0x00,
    11,0x09,NAME_WEAR, /* Complete Name */ 
    7,0xFF, 0x01, 0x00, 0x48, 0xC0, 0x00, 0x00
  };

  /* disable scan response */
  ret = hci_le_set_scan_response_data(0, NULL);

  ret = aci_gap_set_discoverable(ADV_IND, 0,0, RANDOM_ADDR, NO_WHITE_LIST_USE, 
                           sizeof(local_name), local_name, 0, NULL, 0, 0);      //PUBLIC_ADDR

  /* Send Advertising data */
  ret = aci_gap_update_adv_data(23, manuf_data);

  if (ret != BLE_STATUS_SUCCESS)
  {
    return APP_ERROR;
  }
  
  return APP_SUCCESS;
}

//------------------------------------------------------------------------------

void PER_APP_Tick(void)
{
  switch (APP_PER_state)
  {
  case APP_STATUS_ADVERTISEMENT:
    {
    
    }
    break;
  case APP_STATUS_CONNECTED:
    {   
      HDM_service_process();
    }
    break;  
  } 
}

//------------------------------------------------------------------------------

void PER_APP_Disconnect( uint8_t reason )
{
  tBleStatus ret;
  ret = aci_gap_terminate( conn_handle, reason );
  HDM_service_reset();
  if( ret ) PRINT_DEBUG_1("Disconnect err: 0x%X\n", ret );  
}

//------------------------------------------------------------------------------

void PER_APP_remote_command_parser( uint8_t *data, uint8_t length )
{
  switch( data[0] )
  {
  case RCOM_SET_CONF_PART1:   
  case RCOM_SET_CONF_PART2:
    PER_APP_config_update_handler( data, length );
    break;
  case RCOM_GET_CONF:
    if( length == 1 ) CM_uart_tx_command( data, length );
    break;  
  }
}

//--------------------------------------------------------------------------

void PER_APP_config_update_handler( uint8_t *data, uint8_t length )
{
  static uint8_t conf_data[33] = {0};
  
  if( length != 17 ) return;
  
  if( data[0] == RCOM_SET_CONF_PART1 )
    memcpy( conf_data, data, 17 );
  else if( data[0] == RCOM_SET_CONF_PART2 )
  {
    memcpy( &conf_data[17], &data[1], 16 );
    CM_uart_tx_command( conf_data, 33 );
    memset( conf_data, 0, 33 );
  }
}

//--------------------------------------------------------------------------

void PER_APP_requested_config_response( uint8_t *data )
{
  uint8_t conf_data[17];
  uint8_t ret;
  
  conf_data[0] = RCOM_SET_CONF_PART1;
  memcpy( &conf_data[1], &data[0], 16 );
  ret = HDM_service_send_status_data( conf_data, 17 );
  PRINT_DEBUG_2("CFG_P1 ret:%u, D:%u\n", ret, conf_data[1] );
  
  conf_data[0] = RCOM_SET_CONF_PART2;
  memcpy( &conf_data[1], &data[16], 16 );
  ret = HDM_service_send_status_data( conf_data, 17 );
  PRINT_DEBUG_2("CFG_P2 ret:%u D:%u\n", ret, conf_data[1] );
}

//--------------------------------------------------------------------------

void PER_APP_send_OnOff_stream_command( uint8_t command )
{
  uint8_t data;
  if( command )
    data = RCOM_START_STREAM;
  else
    data = RCOM_STOP_STREAM;
    
  CM_reset_rx_buffer();
  CM_uart_tx_command( &data, 1 );    
}

//--------------------------------------------------------------------------

void hci_le_connection_complete_event(uint8_t Status,
                                      uint16_t Connection_Handle,
                                      uint8_t Role,
                                      uint8_t Peer_Address_Type,
                                      uint8_t Peer_Address[6],
                                      uint16_t Conn_Interval,
                                      uint16_t Conn_Latency,
                                      uint16_t Supervision_Timeout,
                                      uint8_t Master_Clock_Accuracy)

{ 
  /* Connection completed */
    
  PRINT_DEBUG_1( "Conn int: %u\n", Conn_Interval );

  conn_handle = Connection_Handle;
  int ret = aci_l2cap_connection_parameter_update_req(conn_handle,
                                                8 /* interval_min*/,
                                                8 /* interval_max */,
                                                0   /* slave_latency */,
                                                400 /*timeout_multiplier*/);
  
  
  /* In order to use an iOS device as receiver, with the ST BlueMS app, please substitute the following function with the previous. */
  /* With iOS only the 8kHz (as audio sampling frequency) version is available */
//  int ret = aci_l2cap_connection_parameter_update_req(conn_handle,
//                                                8 /* interval_min*/,
//                                                17 /* interval_max */,
//                                                0   /* slave_latency */,
//                                                400 /*timeout_multiplier*/);
  
  /* In order to use an Android device version 4 as receiver, with audio sampling frequancy @16kHz, */
  /* please substitute the following function with the previous. */
//  int ret = aci_l2cap_connection_parameter_update_req(conn_handle,
//                                                8 /* interval_min*/,
//                                                8 /* interval_max */,
//                                                0   /* slave_latency */,
//                                                400 /*timeout_multiplier*/);
  
  if (ret != BLE_STATUS_SUCCESS)
  {
    while (1);
  }
  
  APP_PER_state = APP_STATUS_CONNECTED;
    
}

//-----------------------------------------------------------------------------------

void aci_l2cap_connection_update_req_event(uint16_t Connection_Handle,
                                           uint8_t Identifier,
                                           uint16_t L2CAP_Length,
                                           uint16_t Interval_Min,
                                           uint16_t Interval_Max,
                                           uint16_t Slave_Latency,
                                           uint16_t Timeout_Multiplier)
{
  PRINT_DEBUG_1( "L2CAP_Length: %u\n", L2CAP_Length );
  PRINT_DEBUG_1( "Interval_Min: %u\n", Interval_Min );
  PRINT_DEBUG_1( "Interval_Max: %u\n", Interval_Max );
}

//------------------------------------------------------------------------------

void hci_disconnection_complete_event(uint8_t Status,
                                          uint16_t Connection_Handle,
                                          uint8_t Reason)
{
  /*
  if(audio_streaming_active)
    BV_APP_StartStop_ctrl();
  audio_streaming_active = 0;
  */
  
  /* Make the device connectable again. */
  //BluevoiceADPCM_BNRG1_DisconnectionComplete_CB();
  APP_PER_state = APP_STATUS_ADVERTISEMENT;
  
  //DelayMs(100);         //Necessary for stack internal purposes as defined in API reference  
  PRINT_DEBUG_1( "Disconnected: 0x%02X\n", Reason );
  HDM_service_reset();
  
  /*Set module in advertise mode*/
  APP_Status status = PER_APP_Advertise(); 
  if(status != APP_SUCCESS)
  {
    APP_Error_Handler();
  }

}/* end hci_disconnection_complete_event_isr() */

//------------------------------------------------------------------------------

void aci_gatt_proc_timeout_event(uint16_t Connection_Handle)
{
  PER_APP_Disconnect(ERR_RMT_USR_TERM_CONN);
  PRINT_DEBUG_0( "GATT PROC TMEOUT!\n" );
}


#endif //#if ROLE == PERIPHERAL


