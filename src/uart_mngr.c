#include "uart_mngr.h"
#include "misc.h"
#include "clock.h"
#include "BlueNRG1_uart.h"
#include "BlueNRG1_sysCtrl.h"
#include "BlueNRG1_gpio.h"
#include "comm_mngr.h"
#include <stdio.h>
#include <string.h>

void uart_enable_rx_irq(void);

//--------------------------------------------------------------------------

void init_uart(void)
{
  UART_InitType uart_settings;

  SysCtrl_PeripheralClockCmd(CLOCK_PERIPH_UART | CLOCK_PERIPH_GPIO, ENABLE);
    
  GPIO_InitUartTxPin8();
  GPIO_InitUartRxPin11();  
  
  UART_StructInit(&uart_settings);
  uart_settings.UART_FifoEnable = ENABLE;
  UART_Init(&uart_settings);
  /* Interrupt as soon as data is received. */
  UART_RxFifoIrqLevelConfig(FIFO_LEV_1_8);
  UART_Cmd(ENABLE);    
  
  uart_enable_rx_irq();  
    
}

//--------------------------------------------------------------------------

void uart_enable_rx_irq(void)
{
  /* NVIC configuration */
  NVIC_InitType NVIC_InitStructure;
  
  /* Enable the UART Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = UART_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = LOW_PRIORITY;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  
  UART_ITConfig(UART_IT_RX, ENABLE);  
}

//--------------------------------------------------------------------------

void UART_Handler(void)
{
  if( UART_GetITStatus(UART_IT_RX) )
  {
    CM_uart_rx_callback( UART_ReceiveData() );    
  }
}

//--------------------------------------------------------------------------

int putchar(int data)
{
  while (UART_GetFlagStatus(UART_FLAG_TXFF) == SET);
  UART_SendData(data);
  return data;
}

//--------------------------------------------------------------------------
